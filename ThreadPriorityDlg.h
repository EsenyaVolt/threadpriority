﻿
// ThreadPriorityDlg.h: файл заголовка
//

#pragma once

#define IDC_TIMER_CAPACITY 1

// Диалоговое окно CThreadPriorityDlg
class CThreadPriorityDlg : public CDialogEx
{
// Создание
public:
	CThreadPriorityDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_THREADPRIORITY_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
public:
	CString percent1;		// переменные, связанные с элементами диалога
	CString percent2;
	CSliderCtrl slider1;
	CSliderCtrl slider2;
	CProgressCtrl Progress1;
	CProgressCtrl Progress2;
	BOOL sleep1;		// переменные, связанные с чекпоинтами Sleep
	BOOL sleep2;

	int activ;

	static const int ThreadNumber = 2;		// число потоков
	HANDLE Threads[ThreadNumber] = { 0, 0 };		// созданные потоки

	static DWORD WINAPI ThreadFunc(LPVOID pvParam);		// функция потока
	afx_msg void OnBnClickedStart();
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedCancle();
};
