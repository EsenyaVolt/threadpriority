﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется ThreadPriority.rc
//
#define IDD_THREADPRIORITY_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_SLIDER1                     1000
#define IDC_SLIDER2                     1001
#define IDC_PROGRESS1                   1004
#define IDC_PROGRESS2                   1005
#define IDC_START                       1006
#define IDC_CANCLE                      1007
#define IDC_TXT1                        1008
#define IDC_TXT2                        1009
#define IDC_CHECK1                      1010
#define IDC_CHECK2                      1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
