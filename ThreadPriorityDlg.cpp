﻿
// ThreadPriorityDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "ThreadPriority.h"
#include "ThreadPriorityDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define PI 3.141592653589

// Диалоговое окно CThreadPriorityDlg

LPVOID param1 = "first";		// параметр первого потока
LPVOID param2 = "second";		// параметр второго потока

int work1 = 0;		// счетчик первого потока
int work2 = 0;		// счетчик второго потока

BOOL threadSleep1;		// переменная для прерывания первого потока
BOOL threadSleep2;		// переменная для прерывания второго потока

DWORD WINAPI CThreadPriorityDlg::ThreadFunc(LPVOID pvParam)
{
	while (true)
	{
		rand() * rand() * cos(PI);

		if (pvParam == param1)
		{
			work1++;
			if (threadSleep1)
			{
				Sleep(0);
			}
		}

		if (pvParam == param2)
		{
			work2++;
			if (threadSleep2)
			{
				Sleep(0);
			}
		}
	}

	return 0;
}

CThreadPriorityDlg::CThreadPriorityDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_THREADPRIORITY_DIALOG, pParent)
	, percent1(_T("0.0"))
	, percent2(_T("0.0"))
	, sleep1(FALSE)
	, sleep2(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CThreadPriorityDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT1, percent1);
	DDX_Text(pDX, IDC_TXT2, percent2);
	DDX_Control(pDX, IDC_SLIDER1, slider1);
	DDX_Control(pDX, IDC_SLIDER2, slider2);
	DDX_Control(pDX, IDC_PROGRESS1, Progress1);
	DDX_Control(pDX, IDC_PROGRESS2, Progress2);
	DDX_Check(pDX, IDC_CHECK1, sleep1);
	DDX_Check(pDX, IDC_CHECK2, sleep2);
}

BEGIN_MESSAGE_MAP(CThreadPriorityDlg, CDialogEx)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_START, &CThreadPriorityDlg::OnBnClickedStart)
	ON_BN_CLICKED(IDC_CHECK1, &CThreadPriorityDlg::OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_CHECK2, &CThreadPriorityDlg::OnBnClickedCheck2)
	ON_BN_CLICKED(IDC_CANCLE, &CThreadPriorityDlg::OnBnClickedCancle)
	ON_WM_HSCROLL()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// Обработчики сообщений CThreadPriorityDlg

BOOL CThreadPriorityDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	SetProcessAffinityMask(GetCurrentProcess(), 1); // Программе доступно 1 ядро процессора
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST); // Задание приоритета первичного потока
	// TODO: добавьте дополнительную инициализацию

	// Инициализация слайдеров:
	slider1.SetRange(THREAD_PRIORITY_LOWEST, THREAD_PRIORITY_HIGHEST, TRUE);
	slider1.SetTicFreq(1);
	slider1.SetPos(THREAD_PRIORITY_NORMAL);
	slider2.SetRange(THREAD_PRIORITY_LOWEST, THREAD_PRIORITY_HIGHEST, TRUE);
	slider2.SetTicFreq(1);
	slider2.SetPos(THREAD_PRIORITY_NORMAL);

	// задание диапазонов Progress
	Progress1.SetRange(0, 100);
	Progress1.SetPos(0);
	Progress2.SetRange(0, 100);
	Progress2.SetPos(0);

	// начальные значения при запуске программы
	work1 = 0;
	work2 = 0;

	threadSleep1 = false;
	threadSleep2 = false;

	activ = 0;
	
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CThreadPriorityDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

void CThreadPriorityDlg::OnBnClickedStart()
{
	// TODO: добавьте свой код обработчика уведомлений

	switch (activ)
	{
	case 0:
		Threads[0] = CreateThread(NULL, NULL, ThreadFunc, param1, 0, NULL);
		Threads[1] = CreateThread(NULL, NULL, ThreadFunc, param2, 0, NULL);

		SetThreadPriority(Threads[0], slider1.GetPos());		// выставляем приоритет процессов по умолчанию
		SetThreadPriority(Threads[1], slider2.GetPos());

		SetTimer(IDC_TIMER_CAPACITY, 300, NULL);		// таймер подсчета выполненной работы вторичными процессами
		GetDlgItem(IDC_START)->SetWindowTextW(L"Остановка");		// меняем имя кнопки при запуске программы
		activ = 1;
		break;
	case 1:
		KillTimer(1);
		GetDlgItem(IDC_START)->SetWindowTextW(L"Запуск");			// меняем имя кнопки при останове программы
		activ = 2;
		threadSleep1 = true;  threadSleep2 = true;
		SuspendThread(Threads[0]);
		SuspendThread(Threads[1]);
		break;
	case 2:
		SetTimer(IDC_TIMER_CAPACITY, 300, NULL);		// таймер подсчета выполненной работы вторичными процессами
		GetDlgItem(IDC_START)->SetWindowTextW(L"Остановка");		// меняем имя кнопки при запуске программы
		activ = 1;
		threadSleep2 = false; threadSleep2 = false;
		ResumeThread(Threads[0]);
		ResumeThread(Threads[1]);
		sleep1 = false; sleep2 = false;
		break;

	default:
		break;
	}
}

void CThreadPriorityDlg::OnBnClickedCheck1()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData();
	threadSleep1 = sleep1;
}

void CThreadPriorityDlg::OnBnClickedCheck2()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData();
	threadSleep2 = sleep2;
}

void CThreadPriorityDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного
	SetThreadPriority(Threads[0], slider1.GetPos());		// выставляем приоритет первого потока в зависимости от положения слайдера
	SetThreadPriority(Threads[1], slider2.GetPos());		// выставляем приоритет второго потока в зависимости от положения слайдера

	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CThreadPriorityDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного
	if (nIDEvent == IDC_TIMER_CAPACITY)
	{
		float Sum = work1 + work2;

		float ThreadVmestim1 = work1 / Sum * 100.0;
		float ThreadVmestim2 = work2 / Sum * 100.0;

		int FTC = round(ThreadVmestim1);	 // для корректного отображения Progress Bar округляем до целого
		int STC = round(ThreadVmestim2);

		percent1.Format(_T("%0.1f%%"), ThreadVmestim1);		// выводим числовое значение
		percent2.Format(_T("%0.1f%%"), ThreadVmestim2);

		Progress1.SetPos(FTC);		// отображаем числовое значение на ProgressBar
		Progress2.SetPos(STC);

		UpdateData(FALSE);

		//сброс
		work1 = 0;
		work2 = 0;
	}

	CDialogEx::OnTimer(nIDEvent);
}

void CThreadPriorityDlg::OnBnClickedCancle()
{
	// TODO: добавьте свой код обработчика уведомлений

	CDialogEx::OnCancel();
}
